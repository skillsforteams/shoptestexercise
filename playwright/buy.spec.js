// @ts-check
const { test, expect } = require('@playwright/test');

test('buy album on homepage correct amount', async ({ page }) => {
  await page.goto('https://autoprojekt.simplytest.de/');

  //method by href 
  await expect(page.locator('a[href="?add-to-cart=22"]')).toBeVisible();
  //method by ID 
  await expect(page.locator('a[data-product_id="22"]')).toBeVisible();
  
  //alternative selector get by role
  //await expect(page.getByRole('link').and(page.locator('data-product_id=22'))).toBeVisible();


  await page.locator('a[data-product_id="22"]').click();
  
  
  //possbly beeing executed to external function
  await expect(page.locator('a[title="View your shopping cart"] > span.count')).toHaveText('1 item');
  await expect(page.locator('a[title="View your shopping cart"] > span.amount')).toHaveText('15,00 €');

});

test('view cart link', async ({ page }) => {
  // Click the get started link.
  await page.goto('https://autoprojekt.simplytest.de/');
  await page.locator('a[data-product_id="22"]').click();  
  await expect(page.locator('a.added_to_cart')).toBeVisible();  
  await page.locator('a.added_to_cart').click();  
  
  // Expects the URL to contain intro.
  await expect(page).toHaveURL(/.*cart/);

});

test('increase cart amount', async ({ page }) => {
  // test preperation.
  await page.goto('https://autoprojekt.simplytest.de/');
  await page.locator('a[data-product_id="22"]').click();  
  await expect(page.locator('a.added_to_cart')).toBeVisible();  
  await page.locator('a.added_to_cart').click();  
  
  // Expects the URL to contain intro.
  await expect(page).toHaveURL(/.*cart/);
  //
  await expect(page.locator('td[data-title="Total"] > strong > span.amount > bdi')).toHaveText('15,00 €');

  await page.locator('input.qty').fill('2');

  await page.getByRole('button', {name : 'Update cart'}).click()
  await expect(page.locator('td[data-title="Total"] > strong > span.amount > bdi')).toHaveText('30,00 €');
  




});