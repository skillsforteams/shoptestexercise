// @ts-check
const { test, expect } = require('@playwright/test');

test('hompage has 0 items', async ({ page }) => {
  await page.goto('https://autoprojekt.simplytest.de/');
  
  await expect(page.locator('a[title="View your shopping cart"] > span.count')).toHaveText('0 items');
  await expect(page.locator('a[title="View your shopping cart"] > span.amount')).toHaveText('0,00 €');
});



test('has hadline Shop', async ({page}) => {
  await page.goto('https://autoprojekt.simplytest.de/cart')
  await expect(page.locator('p.cart-empty')).toHaveText('Your cart is currently empty.')
  
});
