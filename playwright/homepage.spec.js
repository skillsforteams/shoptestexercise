// @ts-check
const { test, expect } = require('@playwright/test');

test('has title', async ({ page }) => {
  await page.goto('https://autoprojekt.simplytest.de/');

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/Automation Test Project – A great opportunity to test a real-world example…/);
});


test('has hadline Shop', async ({page}) => {
  await page.goto('https://autoprojekt.simplytest.de/')
  await expect(  page.getByRole('heading', { name: 'Shop' })).toBeVisible();
});
