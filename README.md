# ShopTestExercise

This is a test exercise, it´s first occurance is in cypress but it should become a monorepo for different test approaches

## Cypress Development on GitPod 

To make it easy this repo is prepared to be used within GitPod as development Environment

Documentation Used

https://medium.com/@angielohqh/cypress-setup-on-gitpod-io-583b0427c730
- adapted with some extra libraries installed in the docker file

## Gitpod Cheat sheet 
GitPod environment is configured in .gitpod.yml and .gitpod.Dockerfile if u change these filed 
Rebuild command by 
gp validate

## Run cypress tests 
## By gitlab 
### gitlab 
  Commit source code, the runner will run automatically and you will be able to see results. 
  https://gitlab.com/skillsforteams/shoptestexercise/-/pipelines

- run in GitPod by running
### GitPod
   Run all tests
   'npx cypress run 
   Run sungle test
   'npx cypress run --spec cypress/integration/shop/headline.spec.js 
 
### local 
 - install cypress on your local machine

## Run Playwright tests
### GitPod 
   Run all tests
  'npx playwright test

  Showing report could be optimized 
### GitLab 
  Integrated into the GitLab CI as own run job





## Possible Improvements Backlog
  Update cypress version
  Move url configuration to config
  Bundle functions to access core values
  Use cucumber adaption to describe test data
  Use page objects or something like that