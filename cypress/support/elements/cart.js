class Cart {
  constructor() {
    
  }

  visit() {
    //cy.visit('/cart');
  }

  expectTotalAmount(expectedTotalAmount) {
    //cy.visit('https://autoprojekt.simplytest.de/')
    //cy.get('.cart-empty').not.contains('p', 'Your cart is currently empty.')
    cy.scrollTo(0, 300)
    cy.get('td[data-title="Total"]').first().then((el) => {
        const totalAmount = el.text().substring(0, el.text().length-3)
        expect(totalAmount).to.equal(expectedTotalAmount)

    })

       
  }
  /*editNumber(number) {
    /*cy.visit('https://autoprojekt.simplytest.de/')
    cy.get("a.added_to_cart").contains('View cart').click();
    cy.get('input.qty').clear().type(number);
    cy.get('button').contains('Update cart').click();
  }*/


  
  addItem(id){
    cy.visit('https://autoprojekt.simplytest.de/')
    cy.get("a[href='?add-to-cart="+id+"']").click()
  }

}

export default Cart;
