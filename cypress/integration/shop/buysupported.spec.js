// <reference types="cypress" />
import Cart from '../../support/elements/cart';

context('Buy Process supported', () => {

    beforeEach(() => {
      
    })
    /*<p class="cart-empty woocommerce-info">Your cart is currently empty.</p>*/

    it('Supported add item to cart', () => {          
      const cart = new Cart();
      //cart.getTotalAmount();
      cart.addItem(22);
      cy.get("a.added_to_cart").contains('View cart').click();
      cart.expectTotalAmount("15,00");
      //<div class="woocommerce-message" role="alert">Cart updated.	</div>
      
      //updating directly
      cy.get('input.qty').clear().type(2);
      cy.get('button').contains('Update cart').click();
      //wait for update message
      cy.get('div.woocommerce-message').contains('Cart updated. ');

      cart.expectTotalAmount("30,00");
  })
  
})