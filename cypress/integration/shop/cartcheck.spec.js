// <reference types="cypress" />


// check if the cart is initially empty 

context('Shopping Cart', () => {
    
    it('cart should be inititally empty on cart page', () => {      
      cy.visit('https://autoprojekt.simplytest.de/cart/')
      cy.get('.cart-empty').contains('p', 'Your cart is currently empty.')
    })

    it('cart should be initially empty on home page', () => {
      cy.visit('https://autoprojekt.simplytest.de/')
      cy.get('a.cart-contents').contains('span', '0 items')      
    })
    
    it('cart should have 0 amount on homepage', () => {
      cy.visit('https://autoprojekt.simplytest.de/')      
      cy.get('a.cart-contents').contains('span.amount', "0,00")
      cy.get('a.cart-contents > span.amount').contains("0,00")      
    })

    /* Ideas to improve seperate this to "helper" functions as the correct cart amout would be needed frequently */


  })
  

  
