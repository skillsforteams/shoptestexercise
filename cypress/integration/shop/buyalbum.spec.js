// <reference types="cypress" />

context('Buy Process', () => {

    beforeEach(() => {
      cy.visit('https://autoprojekt.simplytest.de/')
    })
    /*<p class="cart-empty woocommerce-info">Your cart is currently empty.</p>*/


    it('Should have correct amount for 1 album via homepage', () => {          
      cy.visit('https://autoprojekt.simplytest.de/')
      cy.get('a.cart-contents').contains('span', '0 items')
      
      cy.get("a[href='?add-to-cart=22'").click()
      cy.get("a.added_to_cart").contains('View cart');

      //cy.get('.woocommerce-products-header__title').contains('h1', 'Shop');    
      cy.scrollTo(0, 0)
      cy.get('a.cart-contents > span.amount').contains("15,00")
      //could not get why it is not working 
      cy.get('span').contains('1 item') 
    })

    it('Should increase amount and total with incorrect selector', () => {         
      /** demonstration of an inaccurate test case using contains and matching also bigger numbers */ 
      cy.visit('https://autoprojekt.simplytest.de/')
      cy.get('a.cart-contents').contains('span', '0 items')
      
      cy.get("a[href='?add-to-cart=22'").click()
      cy.get("a.added_to_cart").contains('View cart').click();      
      
      cy.get('input.qty').clear().type(22);
      cy.get('button').contains('Update cart').click();
      cy.scrollTo(0, 300)
            
      //possible match with 330
      cy.get('span > bdi').should("contain", "30,00");
      
  })

 
  it('Should increase amount and total with incorrect selector', () => {          
    cy.visit('https://autoprojekt.simplytest.de/')
    cy.get('a.cart-contents').contains('span', '0 items')
    
    cy.get("a[href='?add-to-cart=22'").click()
    cy.get("a.added_to_cart").contains('View cart').click();      
    
    cy.get('input.qty').clear().type(2);
    cy.get('button').contains('Update cart').click();
    cy.scrollTo(0, 300)
          
    // better way of asserting to be exactly proof with value
    // possybly could get to an own function

    cy.get('[data-title="Total"]').should("contain", "30,00").then((el) => {
      const totalAmount = el.text().substring(0, el.text().length-3)
      expect(totalAmount).to.equal("30,00")
    })
    
  })
  
})