// <reference types="cypress" />


context('Location', () => {
    beforeEach(() => {
      cy.visit('https://autoprojekt.simplytest.de/')
    })
  /* additional check of page title  */
    it('page title looks correct', () => {      
      cy.title().should('include', 'Automation Test Project')
    })

    it('page title is exactly correct', () => {      
      cy.title().should('eq', 'Automation Test Project – A great opportunity to test a real-world example…')
    })

    /** checking for shop headline */

    /*<h1 class="woocommerce-products-header__title page-title">Shop</h1>*/
    it('headline is Shop by contains', () => {           
      cy.get('.woocommerce-products-header__title').contains('h1', 'Shop');    
    })

    it('headline Shop is there exactly one time', () => {           
      cy.get('h1.woocommerce-products-header__title').contains('h1', 'Shop')
        .should("have.length", 1)

    })




   

  })
  

  
