// <reference types="cypress" />


context('Location', () => {
    beforeEach(() => {
      cy.visit('https://autoprojekt.simplytest.de/')
    })
  
  
    it('should call the correct location', () => {
      // https://on.cypress.io/location
      cy.location().should((location) => {
        expect(location.hash).to.be.empty
        expect(location.href).to.eq('https://autoprojekt.simplytest.de/')
        expect(location.host).to.eq('autoprojekt.simplytest.de')
        expect(location.hostname).to.eq('autoprojekt.simplytest.de')
        expect(location.origin).to.eq('https://autoprojekt.simplytest.de')
        expect(location.pathname).to.eq('/')
        expect(location.port).to.eq('')
        expect(location.protocol).to.eq('https:')
        expect(location.search).to.be.empty
      })
    })
  
    it('should stay on same url', () => {
      // https://on.cypress.io/url
      cy.url().should('eq', 'https://autoprojekt.simplytest.de/')
    })
  
    

    it('should contain the correct title begin', () => {      
      cy.title().should('include', 'Automation Test Project')
    })

    it('should exactly match title', () => {      
      cy.title().should('eq', 'Automation Test Project – A great opportunity to test a real-world example…')
    })

  })
  

  
